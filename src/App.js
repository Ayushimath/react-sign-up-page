
import React from 'react';
// import './reset.css';
import './App.css';
import { Route, Link, Switch } from 'react-router-dom';
import SignUpPage from './SignUpPage';
import Home from './Home';
import Singleproduct from './component/Singleproduct'
import Products from './component/Products';
import axios from 'axios';
class App extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      AllProducts: [],
      selectedId: 1
    }
  }
  componentDidMount() {
    this.getFetchData();

  }

  changeInTitle = (id, title) => {

    console.log(id, title, "updated id and title");
    console.log(this.state.AllProducts);
    let UpdatedItemList = this.state.AllProducts.map((item) => {

      if (item.id == id) {
        item["title"] = title;
      }
      return item;
    });
    console.log(UpdatedItemList);

    this.setState({
      user: UpdatedItemList

    }, () => {
      console.log(this.state.user);
    })

  }
  changeInDescription = (id, description) => {

    console.log(id, description, "updated id and description");
    let UpdatedItemList = this.state.AllProducts.map((item) => {

      if (item.id == id) {
        item["description"] = description;
      }
      return item;
    });
    console.log(UpdatedItemList);

    this.setState({
      user: UpdatedItemList

    }, () => {
      console.log(this.state.user);
    })



  }
  changeInPrice = (id, price) => {

    console.log(id, price, "updated id and price");
    let UpdatedItemList = this.state.AllProducts.map((item) => {

      if (item.id == id) {
        item["price"] = price;
      }
      return item;
    });
    console.log(UpdatedItemList);

    this.setState({
      user: UpdatedItemList

    }, () => {
      console.log(this.state.user);
    })




  }

  getFetchData = () => {
   
    axios.get('https://fakestoreapi.com/products')
    .then(res => res.data)
      .then(
        (data) => {
          this.setState({
            isLoaded: true,
            AllProducts: data
          });
        }).catch((error) => {
          this.setState({
            isLoaded: false,
            error: true
          });
        }
        )
  }


  render() {
    return (
      <div>

        <header>
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="navbar-brand">ShopSo</div>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item active">

                  <Link className='nav-link pages' to="/">Home</Link>
                </li>
                <li className="nav-item">

                  <Link className='nav-link pages' to="/product">product</Link>
                </li>
                <li className="nav-item">

                  <Link className='nav-link pages' to="/signUp"><button id="sign-up-btn">signUp</button></Link>
                </li>
              </ul>
            </div>
          </nav>
          {/* <nav className='navbar navbar-dark bg-primary main-nav'
            style={{
              borderBottom: "solid 1px",
              paddingBottom: "1rem",
            }}
          >
            <div className="navbar-brand" href="#">shopso</div>
            <Link className='pages' to="/">Home</Link>
            <Link className='pages' to="/signUp"><button id="sign-up-btn">signUp</button></Link>

          </nav> */}


        </header>



        <div>
          <Switch>

            <Route exact path="/" component={Home} />
            <Route exact path="/product" render={(props) => (
              <Products changeInTitle={this.changeInTitle}
                changeInDescription={this.changeInDescription}
                changeInPrice={this.changeInPrice} AllProducts={this.state.AllProducts}
                isLoaded={this.state.isLoaded} error={this.state.error} {...props} />)} />
            <Route exact path="/signUp" component={SignUpPage} />
            {/* <Route exact path="/product/:id" component={Singleproduct} /> */}
            <Route exact path="/product/:id" render={(props) => (
              <Singleproduct changeInTitle={this.changeInTitle}
                changeInDescription={this.changeInDescription}
                changeInPrice={this.changeInPrice}
                getProducts={this.getFetchData} AllProducts={this.state.AllProducts}
                isLoaded={this.state.isLoaded} error={this.state.error} {...props} />)} />

          </Switch>
        </div>


      </div>
    )
  }
}

export default App


