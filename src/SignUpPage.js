import React, { Component } from 'react'
import './App.css';
import validator from 'validator';

class SignUpPage extends Component {


    constructor(props) {
        super(props)

        this.state = {
            firstname: "",
            lastname: "",
            age: "",
            gender: "",
            role: "",
            email: "",
            pwd: "",
            repeat_pwd: "",
            checked: false,
            firstname_error: null,
            lastname_error: null,
            email_error: null,
            gender_error: false,
            age_error: null,
            pwd_error: null,
            repeat_pwd_error: null,
            checked_error: "",
            finalDetails: "",
            submittedData: false

        }
    }
    handleFirstNameChange = (event) => {
        this.setState({
            firstname: event.target.value
        })

        // if (!(validator.isAlpha(event.target.value))) {
        //   this.setState({
        //     firstname_error: true
        //   })
        // } else {
        //   this.setState({
        //     firstname_error: false
        //   })

        // }
    }

    handleLastNameChange = (event) => {
        this.setState({
            lastname: event.target.value
        })

        // if (!(validator.isAlpha(event.target.value))) {
        //   this.setState({
        //     lastname_error: true
        //   })
        // } else {
        //   this.setState({
        //     lastname_error: false
        //   })

        // }
    }
    handleAgeChange = (event) => {
        this.setState({
            age: event.target.value
        })

        if (!(validator.isInt(event.target.value, { min: 13, max: 99 }))) {
            this.setState({
                age_error: true
            })
        } else {
            this.setState({
                age_error: false
            })

        }
    }
    handleGenderChange = (event) => {
        this.setState({
            gender: event.target.value
        }, () => {
            if (this.state.gender === "") {
                this.setState({
                    gender_error: true
                }, () => {
                    console.log("gender change", this.state.gender_error)
                })
            } else {
                this.setState({
                    gender_error: false
                })

            }
        })

    }
    handleRoleChange = (event) => {
        this.setState({
            role: event.target.value
        })
    }
    handleEmailChange = (event) => {
        this.setState({
            email: event.target.value
        })
        if (!(validator.isEmail(event.target.value))) {
            this.setState({
                email_error: true
            })
        } else {
            this.setState({
                email_error: false
            })

        }

    }
    handlePwdChange = (event) => {
        this.setState({
            pwd: event.target.value
        })

        // if (!(validator.isStrongPassword(event.target.value, {
        //   minLength: 8,
        //   minLowercase: 1, minUppercase: 1,
        //   minNumbers: 1, minSymbols: 1,
        //   returnScore: false, pointsPerUnique: 1,
        //   pointsPerRepeat: 0.5, pointsForContainingLower: 10,
        //   pointsForContainingUpper: 10, pointsForContainingNumber: 10,
        //   pointsForContainingSymbol: 10
        // }))) {
        //   this.setState({
        //     pwd_error: true
        //   })
        // } else {
        //   this.setState({
        //     pwd_error: false
        //   })

        // }
    }
    handleRepeatPwdChange = (event) => {
        this.setState({
            repeat_pwd: event.target.value
        })

        if ((this.state.pwd) === event.target.value) {
            this.setState({
                repeat_pwd_error: false
            })
        } else {
            this.setState({
                repeat_pwd_error: true
            })

        }
    }

    handleCheckboxChange = () => {
        this.setState({
            checked: (!(this.state.checked))
        }, () => {
            console.log(this.state.checked);
            // if (this.state.checked) {
            //   this.setState({
            //     checked_error: false
            //   })
            // } else {
            //   this.setState({
            //     checked_error: true
            //   })

            // }
        })

    }
    handleSumbit = (event) => {
        //first name
        if (!(validator.isAlpha(this.state.firstname))) {
            this.setState({
                firstname_error: true
            })
        } else {
            this.setState({
                firstname_error: false
            })

        }

        //lastname
        if (!(validator.isAlpha(this.state.lastname))) {
            this.setState({
                lastname_error: true
            })
        } else {
            this.setState({
                lastname_error: false
            })

        }
        //age
        if (!(validator.isInt(this.state.age, { min: 1, max: 99 }))) {
            this.setState({
                age_error: true
            })
        } else {
            this.setState({
                age_error: false
            })

        }

        //gender
        // if (this.state.gender==null) {
        //   this.setState({
        //     gender_error: true
        //   },()=>{console.log(this.state.gender_error)})
        // } else {
        //   this.setState({
        //     gender_error: false
        //   },()=>{console.log(this.state.gender_error)})

        // }

        //email
        if (!(validator.isEmail(this.state.email))) {
            this.setState({
                email_error: true
            })
        } else {
            this.setState({
                email_error: false
            })

        }

        //password
        if (!(validator.isStrongPassword(this.state.pwd, {
            minLength: 8,
            minLowercase: 1, minUppercase: 1,
            minNumbers: 1, minSymbols: 1,
            returnScore: false, pointsPerUnique: 1,
            pointsPerRepeat: 0.5, pointsForContainingLower: 10,
            pointsForContainingUpper: 10, pointsForContainingNumber: 10,
            pointsForContainingSymbol: 10
        }))) {
            this.setState({
                pwd_error: true
            })
        } else {
            this.setState({
                pwd_error: false
            })

        }

        //repeat password
        if ((this.state.pwd) === this.state.repeat_pwd) {
            this.setState({
                repeat_pwd_error: false
            })
        } else {
            this.setState({
                repeat_pwd_error: true
            })

        }

        //checked 
        if (this.state.checked) {
            this.setState({
                checked_error: false
            })
        } else {
            this.setState({
                checked_error: true
            })

        }
        console.log(this.state.gender_error)
        //prevent previous info entered
        event.preventDefault()

        if (
            ((this.state.firstname_error) === false || (this.state.firstname !== "")) &&
            ((this.state.lastname_error) === false || (this.state.lastname !== "")) &&
            ((this.state.email_error) === false || (this.state.email !== "")) &&
            ((this.state.age_error) === false || (this.state.age !== "")) &&
            ((this.state.pwd_error) === false || (this.state.pwd !== "")) &&
            ((this.state.repeat_pwd_error) === false || (this.state.repeat_pwd !== "")) &&
            ((this.state.checked_error) === false || (this.state.checked !== "")) &&
            ((this.state.gender_error) === false || (this.state.gender !== ""))
        ) {

            this.setState({
                finalDetails: {
                    firstname: this.state.firstname,
                    lastname: this.state.lastname,
                    age: this.state.age,
                    gender: this.state.gender,
                    role: this.state.role,
                    email: this.state.email,
                    password: this.state.pwd,
                    repeat_password: this.state.repeat_pwd,
                    checked: this.state.checked

                }
            }, () => {
                this.setState({
                    submittedData: true
                })
                console.log(this.state.finalDetails);

            })
        }


    }
    render() {
        return (


            <div className="container-fluid">
                <div className="row d-flex ">
                    <div className="col-12 col-sm-6 d-flex flex-column order-2 order-sm-1">
                        {/* first name */}
                        <div className="input-group mb-3">
                            <div className="mb-3">
                                <label htmlFor="exampleFormControlInput1" className="form-label">First Name</label>
                                <input type="text" className="form-control" id="exampleFormControlInput1" placeholder="First Name" value={this.state.firstname} onChange={this.handleFirstNameChange} />
                                {this.state.firstname_error && (<div className='incorrect'><div>*Please Enter correct firstname(no spaces)</div>
                                </div>)}
                            </div>
                        </div>

                        {/* Lastname */}
                        <div className="input-group mb-3">
                            <div className="mb-3">
                                <label htmlFor="exampleFormControlInput2" className="form-label">Last Name</label>
                                <input type="text" className="form-control" id="exampleFormControlInput2" placeholder="Last Name" value={this.state.lastname} onChange={this.handleLastNameChange} />
                                {this.state.lastname_error && (<div className='incorrect'><div>*Please Enter correct Lastname(don't enter spaces)</div></div>)}
                            </div>
                        </div>

                        {/* age */}
                        <div className="input-group mb-3">
                            <div className="mb-3">
                                <label htmlFor="exampleFormControlInput3" className="form-label">Age</label>
                                <input type="text" className="form-control" id="exampleFormControlInput3" placeholder="Enter Age" value={this.state.age} onChange={this.handleAgeChange} />
                                {this.state.age_error && (<div className='incorrect'>*Age should not be a charactor
                                    *it should be between 1 and 100</div>)}
                            </div>
                        </div>


                        {/* dropdown role */}
                        <select className="form-select w-50 p-1" aria-label="Default select example" value={this.state.role} onChange={this.handleRoleChange}>
                            <option value="">Role</option>
                            <option value="Developer"> Developer</option>
                            <option value="Senior Developer">Senior Developer</option>
                            <option value="Lead Engineer">Lead Engineer</option>
                            <option value="CTO">CTO</option>
                        </select>

                        {/* checkbox */}

                        <div className="form-check d-flex flex-row justify-content-start align-items-center flex">
                            <input className="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked={this.state.checked}
                                onChange={this.handleCheckboxChange} />
                            <label className="form-check-label" htmlFor="flexCheckChecked">
                                I agree to the terms and conditions.
                            </label>
                        </div>
                        {(this.state.checked_error) && (<div className='incorrect' >*please agree to the terms and conditions</div>)}

                        {/* submit */}
                        <button type="button" className="btn btn-primary w-25" onClick={this.handleSumbit}>Submit</button>

                    </div>

                    <div className="col-sm-6 col-12 order-1 order-sm-2">

                        {/* email  */}
                        <div className="input-group mb-3">
                            <div className="mb-3">
                                <label htmlFor="exampleFormControlInput4" className="form-label">Email</label>
                                <input type="email" className="form-control" id="exampleFormControlInput4" placeholder="Enter Email Id" value={this.state.email}
                                    onChange={this.handleEmailChange} />
                                {this.state.email_error && (<div className='incorrect'>*Please Enter correct Email id</div>)}
                            </div>
                        </div>

                        {/* password  */}
                        <div className="input-group mb-3">
                            <div className="mb-3">
                                <label htmlFor="exampleFormControlInput4" className="form-label">Password</label>
                                <input type="password" className="form-control" id="exampleFormControlInput4" placeholder="Enter Password" name="pwd" value={this.state.pwd}
                                    onChange={this.handlePwdChange} />
                                {this.state.pwd_error && (<div className='incorrect'><div>*Please Enter correct password</div>
                                    <div> * 1 uppercase ,1 lowercase,1 speacial char, required min length of 8 </div></div>)}
                            </div>
                        </div>

                        {/* repeat password */}
                        <div className="input-group mb-3">
                            <div className="mb-3">
                                <label htmlFor="exampleFormControlInput4" className="form-label"> Repeat Password</label>
                                <input type="password" className="form-control" id="exampleFormControlInput4" placeholder="Enter Password" name="repeatpwd" value={this.state.repeat_pwd}
                                    onChange={this.handleRepeatPwdChange} />
                                {this.state.repeat_pwd_error && (<div className='incorrect'>*Password does not match</div>)}
                            </div>
                        </div>


                        {/* gender */}
                        <div className="d-flex flex-row justify-content-start align-items-center">
                            <div className="form-check form-check-inline d-flex flex-row justify-content-center align-items-center" onChange={this.handleGenderChange}>
                                <input className="form-check-input" type="radio" name="gender" id="inlineRadio1" value="Male" />
                                <label className="form-check-label radio-inline" htmlFor="gender">Male</label>
                            </div>
                            <div className="form-check form-check-inline d-flex flex-row justify-content-center align-items-center" onChange={this.handleGenderChange}>
                                <input className="form-check-input" type="radio" name="gender" id="inlineRadio2" value="Female" />
                                <label className="form-check-label radio-inline" htmlFor="gender">Female</label>
                            </div>
                            <div className="form-check form-check-inline d-flex flex-row justify-content-center align-items-center" onChange={this.handleGenderChange}>
                                <input className="form-check-input" type="radio" name="gender" id="inlineRadio3" value="Other" />
                                <label className="form-check-label radio-inline" htmlFor="gender">Other</label>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

            //     <div className="main-container">
            //       {/* {this.state.submittedData && <div className='thanks'>Thank you for registration</div>} */}
            //     <div className="App">
            //     {this.state.submittedData && <div className='thanks'>Thank you for registration</div>}
            //       <div className='main-heading'>Registration Form</div>
            //       <form>

            //         <div className="field">
            //           {/* <label>Firstname:</label> */}
            //           <div className="fa fa-user lock"></div>
            //           <input type='text'className='name' placeholder='Enter First Name' value={this.state.firstname} onChange={this.handleFirstNameChange} />
            //           {this.state.firstname_error && (<div className='incorrect'><div>*Please Enter correct firstname</div>
            //             <div>*please don't enter space or number</div></div>)}
            //         </div>
            //         <div className="field">
            //           {/* <label>Lastname:</label> */}
            //           <div className="fa fa-user lock"></div>
            //           <input type='text' className='name' placeholder='Enter Last Name' value={this.state.lastname} onChange={this.handleLastNameChange} />
            //           {this.state.lastname_error && (<div className='incorrect'><div>*Please Enter correct Lastname</div>
            //             <div>(don't enter spaces)</div></div>)}
            //         </div>

            //         <div className="field">
            //           {/* <label>Age :</label> */}
            //           <div className="fa fa-child fa-lg icon"></div>
            //           <input className='input_tag'type='text' placeholder='Enter Age' value={this.state.age} onChange={this.handleAgeChange} />
            //           {this.state.age_error && (<div className='incorrect'><div>*Age should not be a charactor </div>
            //             <div>*it should be between 1 and 100</div></div>)}
            //         </div>

            //         <div className='gender'>
            //           <label>
            //             {/* <p>Select Gender</p> */}
            //             <div className='gen_field' onChange={this.handleGenderChange}>
            //               <input type="radio" className="gender_el"value="Male" name="gender" /> <label className='gen-label'>Male</label>
            //               <input type="radio" className="gender_el" value="Female" name="gender" /> <label className='gen-label'>Female</label>
            //               <input type="radio" className="gender_el" value="Other" name="gender" /> <label className='gen-label'>other</label>

            //             </div>
            //           </label>
            //         </div>

            //         <div className='field'>
            //           <label>
            //             Select Role : 
            //             <select className='input_tag ' value={this.state.role} onChange={this.handleRoleChange}>
            //               <option value="Developer">Developer</option>
            //               <option value="Senior Developer">Senior Developer</option>
            //               <option value="Lead Engineer">Lead Engineer</option>
            //               <option value="CTO">CTO</option>
            //             </select>
            //           </label>
            //         </div>
            //         <div className='field'>
            //           {/* <label >Email:</label> */}
            //           <div className="fa fa-envelope fa-lg icon"></div>
            //           <input className='input_tag' type="text" placeholder='Enter Email' id="userEmail" value={this.state.email}
            //             onChange={this.handleEmailChange}></input>
            //             {/* <div className="fa fa-envelope"></div> */}
            //           {this.state.email_error && (<div className='incorrect'>*Please Enter correct Email id</div>)}
            //         </div>

            //         <div className='field'>
            //           {/* <label >Password:</label> */}
            //           <i className="fa fa-lock fa-lg icon"></i>
            //           <input className='input_tag' type="password" placeholder="Enter Password" id="pwd" name="pwd" value={this.state.pwd}
            //             onChange={this.handlePwdChange} />
            //             {/* <div className='fas fa-key'></div> */}
            //           {this.state.pwd_error && (<div className='incorrect'><div>*Please Enter correct password</div>
            //           <div> * 1 uppercase ,1 lowercase,1 <div>speacial char, required min length of 8</div> </div></div>)}
            //         </div>
            //         <div className='field'>
            //           {/* <label>Repeat Password:</label> */}
            //           <i className="fa fa-lock fa-lg icon"></i>
            //           <input className='input_tag'type="password" placeholder="Re-enter Password" id="repeatpwd" name="repeatpwd" value={this.state.repeat_pwd}
            //             onChange={this.handleRepeatPwdChange} />
            //             {/* <div className="fa fa-check"></div> */}
            //           {this.state.repeat_pwd_error && (<div className='incorrect'>*Password does not match</div>)}
            //         </div>
            //         <div className='field term-condtion'>

            //             <input
            //               id="term-condition-checkbox"
            //               type="checkbox"
            //               checked={this.state.checked}
            //               onChange={this.handleCheckboxChange}
            //             />
            //             <label>
            //          I agree to terms and conditions
            //           </label>
            //           {(this.state.checked_error) && (<div className='incorrect' >*please agree to the terms and conditions</div>)}
            //         </div>
            //         {/* <div>{this.state.email}</div >
            //         <div>{this.state.email_error}</div > */}
            //         <div className='fields btn-field'>
            //           <button id="btn" onClick={this.handleSumbit}>submit</button>
            //         </div>

            //         {/* <div className='password-condition'>
            //           <h1 id="pwd-heading">Password Conditions</h1>
            //           <p>* It should be atleast 8 charactor long</p>
            //           <p>* It should have one uppercase letter</p>
            //           <p>* It should have one lowercase letter</p>
            //           <p>* It should have one special charactor</p>
            //         </div> */}

            //       </form>

            //     </div>
            //   </div>
        );
    }
}

export default SignUpPage