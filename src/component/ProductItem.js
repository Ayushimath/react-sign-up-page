import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import './ProductCon.css'

class ProductItem extends Component {
 
  render() {

    let product_details;
    if (this.props.element) {
      product_details = this.props.element;
    }
    if (this.props.error) {
      return (
        <div className="col">
          {/* Error: {error.message} */}
          Error in fetching data
        </div>
      );
    } else if (!(this.props.isLoaded)) {
      return (
        <div className="col">
          {/* Loading... */}
          <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
        </div>
      );
    } else {
      return (
        
        <Link to={`/product/${this.props.element.id}`} >
          {/* <div className="container all-products"> */}
          {/* <SingleProductAttach element={this.props.element} /> */}
          {
            this.props.element &&
            (
              <>
                <h6 className='product-title'>{this.props.element.title}</h6>
                <img src={this.props.element.image} className="w-75 img-thumbnail img-fluid" alt={product_details.id} />
                <div>&#36; {`${this.props.element.price}`}</div>
                <div><button type="button" className="btn btn-primary">buy now</button></div>
              </>
            )
          } 
        </Link>
      );
    }
  }
}

export default ProductItem
