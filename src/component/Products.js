import React, { Component } from 'react'
import './ProductCon.css'
import ProductItem from './ProductItem'


class Products extends Component {
  constructor(props) {
    super(props)
    console.log(this.props);
   
  }

  render() {
    // const { error, isLoaded, products } = this.state;
    console.log(this.props);
    let finalproducts;
    if (this.props.AllProducts) {
      finalproducts = this.props.AllProducts;
    }
    console.log(finalproducts);
    if (this.props.error) {
      return (
        <div className="col">
          {/* Error: {error.message} */}
          Error in fetching data
        </div>
      );
    } else if (!(this.props.isLoaded)) {
      return (
        <div className="col">
          {/* Loading... */}

          <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
        </div>
      );
    } else {
      return (
        <div className="container-fluid" >
          <div className="row d-flex justify-content-center">

            {

              this.props.AllProducts && (
                finalproducts.map((product) => {
                  return (
                    <div key={product.id} className="col-sm-4  products-container" >
                      {/* <h6>{product.title}</h6>
                      <div>{`${product.price}`}</div>
                      <div>{product.category}</div>
                      <img src={product.image} class="img-thumbnail" alt='product-image' /> */}
                      <ProductItem changeInTitle={this.props.changeInTitle}
                        changeInDescription={this.props.changeInDescription}
                        changeInPrice={this.props.changeInPrice}
                        element={product} isLoaded={this.props.isLoaded} error={this.props.error}
                      />
                    </div>)
                }
                ))
            }

          </div>
        </div>
      );
    }
  }
}


export default Products

