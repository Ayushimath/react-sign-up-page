import React, { Component } from 'react'
import './ProductCon.css'

class Singleproduct extends Component {
  // constructor(props) {
  //   super(props)
  //   console.log(this.props.match,"helo");
  //   this.state = {
  //     singleproduct: "",
  //     isloaded: false,
  //     error: false
  //   }
  // }

  // componentDidMount() {
  //  this.getProduct();
  // console.log(this.props,"heya");
  // }

  // getProduct(){
  //   console.log(this.props);
  //   const {match}=this.props
  //   const {params}=match
  //   console.log(match);
  //   const {id}=params
  //   if(this.props.match.params.id){
  //   fetch(`https://fakestoreapi.com/products/${id}`)
  //     .then(res => res.json())
  //     .then((data) => {
  //       this.setState({
  //         singleproduct: data,
  //         isLoaded: true
  //       })
  //       console.log(data);
  //     }).catch((err) => {
  //       this.setState({
  //         isLoaded: false,
  //         error: true

  //       })
  //     })
  //   }
  // }
  handleTitleChange=(event)=>{
    console.log(event.target.value);
    this.props.changeInTitle(this.props.match.params.id,event.target.value);
  }
  handlePriceChange=(event)=>{
    console.log(event.target.value);
    this.props.changeInPrice(this.props.match.params.id,event.target.value);
  }
  handleDescriptionChange=(event)=>{
    console.log(event.target.value);
    this.props.changeInDescription(this.props.match.params.id,event.target.value);
    
  }
  render() {
    console.log(this.props);
    const {match}=this.props
    const {params}=match
    console.log(match);
    const {id}=params
    console.log("real",id);
    let matchId=this.props.match.id;
    console.log(matchId);
    console.log(this.props.AllProducts);
    let productData=this.props.AllProducts.filter((eachItem)=>{
      if(eachItem.id==id){
        return eachItem;
      }
    });

    console.log("heya",productData);
    let product_details;
    if (productData) {
      product_details = productData.pop();
    }
    if (this.props.error) {
      return (
        <div className="col">
          {/* Error: {error.message} */}
          Error in fetching data
        </div>
      );
    } else if (!(this.props.isLoaded)) {
      return (
        <div className="col">
          {/* Loading... */}
          <div class="lds-ring"><div></div><div></div><div></div><div></div></div>

        </div>
      );
    } else {
      return (
        <div className="container single-pro-con">

          {
            productData &&
            (<div key={product_details.id} className="col-sm-3 singleProduct" >
              <h4 htmlFor='title'>{product_details.title}</h4>
              <input onChange={this.handleTitleChange} id='title'/>
              <img src={product_details.image} className="img-thumbnail" alt={product_details.id} />
              <div htmlFor='price'>{`${product_details.price}`}</div>
              <input onChange={this.handlePriceChange} id='price'/>
              <div htmlFor='description'>{product_details.description}</div>
              <input onChange={this.handleDescriptionChange} id='description'/>
              <div><button type="button" class="btn btn-primary">buy now</button></div>
            </div>)
          }
        </div>
      );
    }
  }
}


export default Singleproduct